/*
 * $File: init_proj.cc
 * $Date: Wed May 01 15:18:54 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "stitcher_impl.hh"

void ImageStitcher::Impl::choose_identity_image(ComposedImage &composed) const {
	composed.identity_image_idx = composed.component[0].image_idx;

	Vector3D center;
	// calculate weighted center
	{
		for (auto &c: composed.component) {
			Vector3D p00(c.homography.trans(0, 0)),
					 p01(c.homography.trans(0, 1)),
					 p10(c.homography.trans(1, 0)),
					 p11(c.homography.trans(1, 1)),
					 pa(p00, p10),
					 pb(p00, p11),
					 pc(p00, p01);
			real_t area = pa.cross(pb).mod() + pb.cross(pc).mod();
			center += c.homography.trans(0.5, 0.5).get_normalized() * area;
		}
		if (center.mod() < 0.1) {
			log_printf("could not compute projection center");
			return;
		}
	}
	ImageComponent *id_img = nullptr;
	Vector2D center_on_img;
	// find image most close to center
	{
		real_t max_dot = -1;
		for (auto &c: composed.component) {
			Vector2D t(c.homography.get_inv().trans_normalized(center));
			if (t.x >= 0 && t.x <= 1 && t.y >= 0 && t.y <= 1) {
				Vector3D pt(c.homography.trans(0.5, 0.5));
				real_t dot = pt.dot(center) / pt.mod();
				if (dot > max_dot) {
					max_dot = dot;
					id_img = &c;
					center_on_img = t;
				}
			}
		}
	}
	if (!id_img) {
		log_printf("could not compute projection center");
		return;
	}

	Vector3D kpt0 = id_img->homography.trans(center_on_img),
			 kpt1 = id_img->homography.trans(center_on_img + Vector2D(0.5, 0)),
			 kpt2 = id_img->homography.trans(center_on_img + Vector2D(0, 0.5));
	real_t
		mat_from[9] = {
			kpt0.x, kpt1.x, kpt2.x,
			kpt0.y, kpt1.y, kpt2.y,
			kpt0.z, kpt1.z, kpt2.z},
		mat_to[9] = {
			0, 0.5, 0,
			0, 0, 0.5,
			1, 1, 1};

	Homography t = Homography(mat_to) * Homography(mat_from).get_inv();
	for (auto &c: composed.component)
		c.homography = t * c.homography;
	composed.identity_image_idx = id_img->image_idx;
	log_printf("center image: %d (%.3f, %.3f)", int(id_img->image_idx),
			center_on_img.x, center_on_img.y);
}


void ImageStitcher::Impl::calc_projection_param(ComposedImage &composed) const {

	// decide projection method
	{
		vector<real_t> norm_angle;
		real_t min_w = numeric_limits<real_t>::max();
		for (auto &i: composed.component) {
			update_min(min_w, i.homography.min_w());
			Vector3D p0 = i.homography.trans(0, 0),
					 p1 = i.homography.trans(0, 1),
					 p2 = i.homography.trans(1, 0),
					 norm = (p1 - p0).cross(p2 - p0);
			norm_angle.push_back(atan2(norm.y, norm.z));
		}

		bool use_flat = true;
		real_t max_normal_angle = numeric_limits<real_t>::lowest();
		for (size_t i = 0; use_flat && i < norm_angle.size(); i ++)
			for (size_t j = i + 1; j < norm_angle.size(); j ++) {
				real_t d = fmod(fabs(norm_angle[i] - norm_angle[j]), M_PI * 2);
				d = min(d, M_PI * 2 - d);
				update_max(max_normal_angle, d);
				if (d > m_stitcher.m_proj_flat_max_angle) {
					log_printf("non-flat projection due to: "
							"normal_angle = %.3lf[deg]",
							d / M_PI * 180);
					use_flat = false;
					break;
				}
			}

		if (use_flat &&
				min_w < m_stitcher.m_proj_flat_minw &&
				max_normal_angle > m_stitcher.m_proj_cylin_min_agl) {

			use_flat = false;
			log_printf("non-flat projection due to: "
					"min_w=%.4f, normal_angle = %.3lf[deg]",
					min_w, max_normal_angle / M_PI * 180);
		}

		if (use_flat) {
			composed.projection_method = ProjectionMethod::FLAT;
			log_printf("projection method: flat");
		}
		else {
			composed.projection_method = ProjectionMethod::CYLINDRICAL;
			log_printf("projection method: cylindrical");
			horiz_straighten(composed);
		}
	}
}

void ImageStitcher::Impl::calc_projection_range(ComposedImage &composed) const {
	auto &c = composed;
	homo2proj_t homo2proj;
	using namespace stitcher_impl::projector;
	if (c.projection_method == ProjectionMethod::FLAT)
		homo2proj = flat::homo2proj;
	else {
		assert(c.projection_method == ProjectionMethod::CYLINDRICAL);
		homo2proj = cylindrical::homo2proj;
		rotate_for_cylindrical_proj(composed);
	}
	for (auto &p: c.component) 
		p.homography_inv = p.homography.get_inv();

	c.proj_min.x = c.proj_min.y = numeric_limits<real_t>::max();
	c.proj_max.x = c.proj_max.y = numeric_limits<real_t>::lowest();
	for (auto &p: c.component) {
		Vector2D p0 = homo2proj(p.homography.trans(0, 0)),
				 p1 = homo2proj(p.homography.trans(0, 1)),
				 p2 = homo2proj(p.homography.trans(1, 0)),
				 p3 = homo2proj(p.homography.trans(1, 1));

		update_min(c.proj_min.x, p0.x, p1.x, p2.x, p3.x);
		update_min(c.proj_min.y, p0.y, p1.y, p2.y, p3.y);
		update_max(c.proj_max.x, p0.x, p1.x, p2.x, p3.x);
		update_max(c.proj_max.y, p0.y, p1.y, p2.y, p3.y);
	}
	c.proj_min.x -= 0.025;
	c.proj_min.y -= 0.025;
	c.proj_max.x += 0.025;
	c.proj_max.y += 0.025;

	if (c.update_proj_range)
		c.update_proj_range();
}

void ImageStitcher::Impl::horiz_straighten(ComposedImage &composed) const {
	vector<Vector3D> center;
	for (auto &c: composed.component) {
		center.push_back(c.homography.trans(0.5, 0.5, 1));
		center.back().normalize();
	}

	if (center.size() >= 2) {
		Homography t = Homography::calc_trans_for_cylindrical_proj(center);
		for (auto &c: composed.component)
			c.homography = t * c.homography;
	}

}

void ImageStitcher::Impl::rotate_for_cylindrical_proj(ComposedImage &composed) const {
	struct Arc {
		bool omitted = false;
		real_t left, right;
		Arc(real_t a, real_t b) {
			if (a > b)
				swap(a, b);
			if (b - a > M_PI) {
				left = b - M_PI * 2;
				right = a;
			} else {
				left = a;
				right = b;
			}
		}

		bool contain(real_t x, real_t &lt, real_t &rt) const {
			lt = left;
			rt = right;
			real_t d = ifloor((x - lt) / (M_PI * 2)) * M_PI * 2;
			lt += d; rt += d;
			return lt <= x && x <= rt;
		}
		bool intersect(const Arc &c) const {
			real_t l, r;
			return contain(c.left, l, r) || contain(c.right, l, r)
				|| c.contain(left, l, r) || c.contain(right, l, r);
		}
	};
	vector<Arc> arc;
	for (auto &c: composed.component) {
		real_t agl[4];
		int num = 0;
		auto proc = [&](real_t x0, real_t y0) {
			Vector3D homo = c.homography.trans(x0, y0);
			agl[num ++] = atan2(homo.y, homo.z);
		};
		proc(0, 0); proc(0, 1);
		proc(1, 0); proc(1, 1);
		real_t max_d = -1, cur_l = -1, cur_r = -1;
		for (int i = 0; i < 4; i ++)
			for (int j = 0; j < i; j ++) {
				real_t d = fmod(fabs(agl[i] - agl[j]), M_PI * 2);
				d = min(d, M_PI * 2 - d);
				if (d > max_d) {
					max_d = d;
					cur_l = agl[i];
					cur_r = agl[j];
				}
			}
		arc.emplace_back(cur_l, cur_r);
	}

	real_t left = arc[0].left, right = arc[0].right;
	arc[0].omitted = true;

	for (bool found = true; found; ) {
		found = false;
		for (auto &c: arc)
			if (!c.omitted) {
				real_t l1, r1;
				if (c.contain(left, l1, r1)) {
					left = l1;
					c.omitted = true;
					found = true;
				}
				if (c.contain(right, l1, r1)) {
					right = r1;
					c.omitted = true;
					found = true;
				}
			}
	}

	real_t agl = 0;	// rotate this vector to -x axis
	if (right - left >= M_PI * 2) {
		composed.update_proj_range = [&composed]() {
			composed.proj_min.y = -M_PI;
			composed.proj_max.y = M_PI;
		};
		composed.update_img_proj_range = []
			(real_t &xmin, real_t &ymin, real_t &xmax, real_t &ymax) {
				if (ymax - ymin > M_PI) {
					ymin = -M_PI;
					ymax = M_PI;
				}
			};

		bool ok = false;
		// find angle to break cylinder
		auto &comp = composed.component;
		for (size_t i = 0; i < comp.size() && !ok; i ++) {
			auto &conn = m_mst_connected[comp[i].image_idx];
			for (size_t j = 0; j < i; j ++)
				if (arc[i].intersect(arc[j]) && !conn[comp[j].image_idx]) {
					log_printf("detected 360-degree panorama, "
							"break at %d <-> %d", int(comp[i].image_idx),
							int(comp[j].image_idx));
					agl = arc[i].left;
					ok = true;
					break;
				}
		}
		assert(ok);
	} else
		agl = (left + right) / 2 + M_PI;

	real_t cos_agl = cos(-M_PI - agl),
		   sin_agl = sin(-M_PI - agl),
		   mat[9] = {
			   1, 0, 0,
			   0, cos_agl, sin_agl,
			   0, -sin_agl, cos_agl};
	Homography t(mat);
	for (auto &c: composed.component)
		c.homography = t * c.homography;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
