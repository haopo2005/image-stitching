/*
 * $File: mat.cc
 * $Date: Thu May 02 14:27:48 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief matrix operations used in ImageStitcher
 */

#include <Eigen/Dense>

#include "stitcher_mat.hh"
#include "utils.hh"

using namespace std;
using namespace stitcher_impl;
using namespace Eigen;

Homography Homography::calc_from_match(
		const MatchedPointArray &match) {

	assert(match.size() >= 4);

	MatrixXd coeff(match.size() * 2, 8);
	VectorXd b(match.size() * 2);
	int r = 0;
	for (auto &p: match) {
		coeff.row(r) << -p.x0, -p.y0, -1, 0, 0, 0, p.x0 * p.x1, p.y0 * p.x1;
		coeff.row(r + 1) << 0, 0, 0, -p.x0, -p.y0, -1, p.x0 * p.y1, p.y0 * p.y1;
		b(r, 0) = -p.x1;
		b(r + 1, 0) = -p.y1;
		r += 2;
	}
	VectorXd ans = coeff.jacobiSvd(ComputeThinU | ComputeThinV).solve(b);
	Homography ret;
	for (int i = 0; i < 8; i ++)
		ret.mat[i] = ans[i];
	ret.mat[8] = 1;
	ret.normalize();
	return ret;
}

Homography Homography::calc_trans_for_cylindrical_proj(
		const vector<Vector3D> &plane) {

	Vector3D norm;
	// compute the normal vector
	{
		assert(plane.size() >= 2);

		MatrixXd coeff(plane.size(), 2);
		VectorXd b(plane.size());

		for (size_t i = 0; i < plane.size(); i ++) {
			coeff.row(i) << plane[i].y, plane[i].z;
			b[i] = -plane[i].x;
		}
		VectorXd ans = coeff.jacobiSvd(ComputeThinU | ComputeThinV).solve(b);
		norm.x = 1;
		norm.y = ans[0];
		norm.z = ans[1];
	}

	Vector3D ax_y = norm.cross(Vector3D(0, 0, -1)),
			 ax_z = norm.cross(ax_y);
	Matrix2f yzinv_core;
	yzinv_core << ax_y.y, ax_z.y, ax_y.z, ax_z.z;
	yzinv_core = yzinv_core.inverse().eval();
	Matrix3f yzinv;
	yzinv.row(1) << 0, yzinv_core(0, 0), yzinv_core(0, 1);
	yzinv.row(2) << 0, yzinv_core(1, 0), yzinv_core(1, 1);
	Eigen::Matrix3f yz;
	yz << 0, ax_y.x, ax_z.x, 0, ax_y.y, ax_z.y, 0, ax_y.z, ax_z.z;
	yz *= yzinv;

	Homography ret;
	ret.set_as_identity();
	ret.mat[0] = 1 - yz(0, 0);
	ret.mat[1] = - yz(0, 1);
	ret.mat[2] = - yz(0, 2);

	return ret;
}


void Homography::set_as_identity() {
	memset(mat, 0, sizeof(mat));
	mat[0] = mat[4] = mat[8] = 1;
}

Homography Homography::operator * (const Homography &rhs) const {
	Homography ret;
	real_t *rst = ret.mat;
	const real_t *h0 = mat, *h1 = rhs.mat;
	rst[0] = h0[0] * h1[0] + h0[1] * h1[3] + h0[2] * h1[6];
	rst[1] = h0[0] * h1[1] + h0[1] * h1[4] + h0[2] * h1[7];
	rst[2] = h0[0] * h1[2] + h0[1] * h1[5] + h0[2] * h1[8];
	rst[3] = h0[3] * h1[0] + h0[4] * h1[3] + h0[5] * h1[6];
	rst[4] = h0[3] * h1[1] + h0[4] * h1[4] + h0[5] * h1[7];
	rst[5] = h0[3] * h1[2] + h0[4] * h1[5] + h0[5] * h1[8];
	rst[6] = h0[6] * h1[0] + h0[7] * h1[3] + h0[8] * h1[6];
	rst[7] = h0[6] * h1[1] + h0[7] * h1[4] + h0[8] * h1[7];
	rst[8] = h0[6] * h1[2] + h0[7] * h1[5] + h0[8] * h1[8];
	ret.normalize();
	return ret;
}

Homography Homography::get_inv() const {
	Matrix3f m;
	const real_t *h = mat;
	m << h[0], h[1], h[2],
	   h[3], h[4], h[5],
	   h[6], h[7], h[8];
	m = m.inverse().eval();
	Homography ret;
	real_t *rst = ret.mat;
	for (int i = 0; i < 3; i ++)
		for (int j = 0; j < 3; j ++)
			*(rst ++) = m(i, j);
	ret.normalize();
	return ret;
}

void Homography::normalize() {
	real_t fac = 0;
	for (int i = 0; i < 9; i ++)
		fac += mat[i] * mat[i];
	fac = 9 / sqrt(fac);
	for (int i = 0; i < 9; i ++) {
		mat[i] *= fac;
		assert(isnormalr(mat[i]));
	}
}


vector<real_t> gain_compensator_impl::solve_eqn(
		const vector<Equation> &eqn_group, int nr_unknown) {

	vector<real_t> result(nr_unknown);
	if (!nr_unknown)
		return result;

	assert(nr_unknown <= int(eqn_group.size()));

	MatrixXd coeff = MatrixXd::Zero(nr_unknown, nr_unknown);
	VectorXd b = VectorXd::Zero(nr_unknown);
	for (auto &eqn: eqn_group) {
		for (auto &main_term: eqn.term) {
			real_t c = main_term.coeff;
			for (auto &t: eqn.term)
				coeff(main_term.idx, t.idx) += t.coeff * c;
			b[main_term.idx] -= c * eqn.c;
		}
	}

	VectorXd ans = coeff.colPivHouseholderQr().solve(b);
	for (int i = 0; i < nr_unknown; i ++)
		result[i] = ans[i];

	return result;
}

vector<real_t> stitcher_impl::solve_linear_eqn(
		const ::Matrix<real_t> &A, const vector<real_t> &b) {
	assert(A.height() == int(b.size()) && A.height() == A.width());
	MatrixXd ma(A.height(), A.width());
	for (int i = 0; i < A.height(); i ++)
		for (int j = 0; j < A.width(); j ++)
			ma(i, j) = A.at(i, j);
	VectorXd mb(b.size());
	for (size_t i = 0; i < b.size(); i ++)
		mb[i] = b[i];

	VectorXd ans = ma.inverse() * mb;
	vector<real_t> ret(b.size());
	for (size_t i = 0; i < b.size(); i ++)
		ret[i] = ans[i];
	return ret;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

