/*
 * $File: debug.cc
 * $Date: Tue Apr 30 00:18:24 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "stitcher_impl.hh"
#include "draw.hh"
#include "config.hh"
#include "exc.hh"

#include <cstdio>

void ImageStitcher::Impl::draw_match(
		const MatchedPointArray &match, const Homography &trans,
		size_t idx0, size_t idx1) const {

	const Image &img0 = m_feature_image[idx0],
				&img1 = m_feature_image[idx1];
	constexpr int GAP_WIDTH = 10;
	Image target = Image::create(img0.width() + img1.width() + GAP_WIDTH,
			max(img0.height(), img1.height()));
	int img1_c0 = img0.width() + GAP_WIDTH;

	draw::image(target, Point(0, 0), img0);
	draw::image(target, Point(0, img1_c0), img1);

	for (auto &m: match) 
		draw::line(target,
				Point(ifloor(img0.height() * m.x0),
					ifloor(img0.width() * m.y0)),
				Point(ifloor(img1.height() * m.x1),
					ifloor(img1.width() * m.y1) + img1_c0));

	char fname[255];
	sprintf(fname, DEBUG_DIR"match-%.2d-%.2d", int(idx0), int(idx1));
	target.save("%s.png", fname);
	log_printf("saved image of matched points to %s.png", fname);
	strcat(fname, ".txt");
	FILE *fout = fopen(fname, "w");
	if (!fout)
		throw IstitcherError("failed to open file %s: %m", fname);
	fprintf(fout, "# homography matrix\n");
	for (int i = 0; i < 3; i ++) {
		for (int j = 0; j < 3; j ++)
			fprintf(fout, "%.5f ", trans.get(i, j));
		fprintf(fout, "\n");
	}
	fprintf(fout, "# matched pairs p0 p1 p0'\n");
	for (auto &p: match) {
		Vector2D p0t = trans.trans_normalized(p.x0, p.y0);
		fprintf(fout, "(%.5lf, %.5lf) (%.5lf %.5lf) (%.5lf %.5lf)\n",
				p.x0, p.y0, p.x1, p.y1, p0t.x, p0t.y);
	}
	fclose(fout);
	log_printf("saved matched points to %s", fname);
}

void ImageStitcher::Impl::dump_homo_point(const ComposedImage &composed) const {
	static int num;
	char fname[255];
	sprintf(fname, DEBUG_DIR"homo-%d.obj", num);
	FILE *fout = fopen(fname, "w");
	if (!fout)
		throw IstitcherError("failed to open %s: %m", fname);
	int vtx_cnt = 1;
	for (auto &c: composed.component) {
		fprintf(fout, "o image_%d\n", int(c.image_idx));
		for (int x: {0, 1})
			for (int y: {0, 1}) {
				Vector3D p = c.homography.trans(x, y);
				fprintf(fout, "v %.6le %.6le %.6le\n",
						p.x, p.y, p.z);
			}
		fprintf(fout, "f %d %d %d %d\n",
				vtx_cnt, vtx_cnt + 1, vtx_cnt + 3, vtx_cnt + 2);
		vtx_cnt += 4;
	}
	fclose(fout);
	log_printf("homo point dumped to %s", fname);

	sprintf(fname, DEBUG_DIR"homo-%d.txt", num ++);
	fout = fopen(fname, "w");
	if (!fout)
		throw IstitcherError("failed to open %s: %m", fname);
	for (auto &c: composed.component) {
		fprintf(fout, "image %d homography matrix:\n", int(c.image_idx));
		for (int i = 0; i < 3; i ++) {
			for (int j = 0; j < 3; j ++)
				fprintf(fout, "%.4lf ", c.homography.get(i, j));
			fprintf(fout, "\n");
		}
		fprintf(fout, "image %d inverse homography matrix:\n",
				int(c.image_idx));
		for (int i = 0; i < 3; i ++) {
			for (int j = 0; j < 3; j ++)
				fprintf(fout, "%.4lf ", c.homography_inv.get(i, j));
			fprintf(fout, "\n");
		}
	}
	fclose(fout);
	log_printf("homography matrices dumped to %s", fname);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
