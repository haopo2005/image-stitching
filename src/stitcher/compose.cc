/*
 * $File: compose.cc
 * $Date: Tue Apr 30 00:18:05 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "stitcher_impl.hh"
#include "exc.hh"
#include "blender/linear.hh"
#include "blender/multiband.hh"
#include "resizer/gaussian_bilinear.hh"

ImageArray ImageStitcher::Impl::stitch(const ImageArray &image) const {
	ImageArray ret;
	for (auto &c: m_composed)
		ret.push_back(do_compose(c, image));
	return ret;
}

Image ImageStitcher::Impl::do_compose(
		const ComposedImage &composed, const ImageArray &input_image) const {

	using namespace stitcher_impl::projector;
	switch (composed.projection_method) {
		case ProjectionMethod::FLAT:
			return do_compose_with_homotrans<flat::homo2proj, flat::proj2homo>(
						composed, input_image);
		case ProjectionMethod::CYLINDRICAL:
			return do_compose_with_homotrans<
							cylindrical::homo2proj, cylindrical::proj2homo>(
						composed, input_image);
	}
	// should never get here
	throw IstitcherError("invalid projection method");
}

template <projector::homo2proj_t homo2proj, projector::proj2homo_t proj2homo>
Image ImageStitcher::Impl::do_compose_with_homotrans(
		const ComposedImage &composed, const ImageArray &input_image_orig)
		const {

	const Image& id_img = input_image_orig[composed.identity_image_idx];

	Vector2D id_img_range = homo2proj(Vector3D(1, 1, 1)) -
		homo2proj(Vector3D(0, 0, 1));
	real_t x_len = composed.proj_max.x - composed.proj_min.x,
		   y_len = composed.proj_max.y - composed.proj_min.y,
		   x_per_pixel = id_img_range.x / id_img.height(),
		   y_per_pixel = id_img_range.y / id_img.width(),
		   target_height = x_len / x_per_pixel,
		   target_width = y_len / y_per_pixel;
	ImageArray input_image;
	// resize if necessary
	{
		real_t f = sqrt(real_t(m_stitcher.m_output_image_max_area) /
			(target_width * target_height));
		update_min(f, m_stitcher.m_output_image_max_size /
				max(target_width, target_height));
		if (f < 1) {
			target_width *= f;
			target_height *= f;
			x_per_pixel /= f;
			y_per_pixel /= f;
			log_printf("resize input image: %.4f", f);
			GaussianBilinearResizer resizer;
			for (auto &i: input_image_orig)
				input_image.push_back(resizer.resize(i, f));
		} else
			input_image = input_image_orig;
	}
	Image target = Image::create(
			iceil(target_width) + 2, iceil(target_height) + 2);
	log_printf("output image size: %dx%d", target.width(), target.height());

	ImageArray gained, feature_image;
	for (auto &c: composed.component) {
		gained.push_back(input_image[c.image_idx]);
		feature_image.push_back(m_feature_image[c.image_idx]);
	}
	gained = composed.gain_compensator.run(gained, feature_image);

	blend<homo2proj, proj2homo>(target, x_per_pixel, y_per_pixel,
				composed.proj_min.x, composed.proj_min.y,
				gained, composed);

	return target;
}


template <projector::homo2proj_t homo2proj, projector::proj2homo_t proj2homo>
void ImageStitcher::Impl::blend(Image &target, 
		real_t scale_x, real_t scale_y,
		real_t offset_x, real_t offset_y,
		const ImageArray &input_image,
		const ComposedImage &composed) const {

	auto &component = composed.component;
	assert(input_image.size() == component.size());

	BlenderBase *blender;
	if (m_stitcher.m_blend_method == 0) {
		blender = new LinearBlender;
		log_printf("blending method: linear");
	} else {
		blender = new MultibandBlender;
		log_printf("blending method: multiband");
	}
	Matrix<Vector2D> orig_pos;
	static int call_num = 0;
	for (size_t cmpnt_idx = 0; cmpnt_idx < component.size(); cmpnt_idx ++) {
		auto &cur_comp = component[cmpnt_idx];

		int xmin, xmax, ymin, ymax;
		// find projection range
		{ 
			real_t xmin_proj = numeric_limits<real_t>::max(),
				   xmax_proj = numeric_limits<real_t>::lowest(),
				   ymin_proj = xmin_proj, ymax_proj = xmax_proj;

			auto proj = [&](real_t x, real_t y) {
				auto p = homo2proj(cur_comp.homography.trans(x, y));
				update_min(xmin_proj, p.x);
				update_max(xmax_proj, p.x);
				update_min(ymin_proj, p.y);
				update_max(ymax_proj, p.y);
			};
			proj(0, 0); proj(0, 1);
			proj(1, 0); proj(1, 1);
			if (composed.update_img_proj_range)
				composed.update_img_proj_range(
						xmin_proj, ymin_proj, xmax_proj, ymax_proj);
			xmin = ifloor((xmin_proj - offset_x) / scale_x);
			ymin = ifloor((ymin_proj - offset_y) / scale_y);
			xmax = iceil((xmax_proj - offset_x) / scale_x + 1);
			ymax = iceil((ymax_proj - offset_y) / scale_y + 1);
		}

		int height = xmax - xmin, width = ymax - ymin;

		orig_pos.set_dimension(width, height);
		for (int i = 0; i < height; i ++) {
			Vector2D proj((i + xmin) * scale_x + offset_x);
			for (int j = 0; j < width; j ++) {
				proj.y = (j + ymin) * scale_y + offset_y;
				auto &p = (orig_pos.at(i, j) = 
						cur_comp.
						homography_inv.trans_normalized(proj2homo(proj)));
				if (!p.is_nan() && (p.x < 0 || p.x >= 1 || p.y < 0 || p.y >= 1))
					p = p.NaN();
			}
		}
		if (m_stitcher.m_save_transformed_img) {
			assert(0); // not needed now
		}
		blender->add_image(Point(xmin, ymin), orig_pos, input_image[cmpnt_idx]);
	}
	if (m_stitcher.m_save_transformed_img) {
		call_num ++;
		log_printf("transformed image saved");
	}
	blender->run(target);
	delete blender;
}


// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
