/*
 * $File: init.cc
 * $Date: Thu May 02 15:42:09 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "stitcher_impl.hh"

#include <algorithm>
#include <random>

static MatchedPointArray find_pairwise_feature_match(
		const FeatureMatcherBase &matcher0, const FeatureMatcherBase &matcher1);
static bool within_01(const Vector2D &v) {
	return v.x >= 0 && v.x < 1 && v.y >= 0 && v.y < 1;
}


void ImageStitcher::Impl::init(const FeatureMatcherPtrArray &matcher,
		const ImageArray &feature_image) {

	m_feature_image = feature_image;

	calc_pairwise_match(matcher);
	calc_mst();

	log_printf("input images splited into %d components",
			int(m_composed.size()));

	for (auto &composed: m_composed) {
		choose_identity_image(composed);
		calc_projection_param(composed);
		bundle_adjust(composed);
		calc_projection_range(composed);

		if (m_stitcher.m_dump_homo_point)
			dump_homo_point(composed);

		auto &c = composed.component;
		auto &gc = composed.gain_compensator;

		for (size_t i = 0; i < c.size(); i ++)
			for (size_t j = 0; j < c.size(); j ++) {
				size_t v0 = c[i].image_idx, v1 = c[j].image_idx;
				auto &p = m_pairwise_match[v0][v1];
				if (!p.matched.empty()) 
					gc.add(i, j, p.trans);
			}
	}
}

void ImageStitcher::Impl::bundle_adjust(ComposedImage &composed) const {

	BundleAdjustor adjustor;
	adjustor.m_outlier_thresh = m_stitcher.m_ba_conf_thresh;
	auto &component = composed.component;
	for (size_t i = 0; i < component.size(); i ++) {
		auto &comp0 = component[i];
		auto &homo0 = comp0.homography;
		auto homo0_inv = homo0.get_inv();
		for (size_t j = 0; j < i; j ++) {
			auto &comp1 = component[j];
			auto &homo1 = comp1.homography;
			auto homo1_inv = homo1.get_inv();
			auto &match =
				m_pairwise_match[comp0.image_idx][comp1.image_idx].matched;

			vector<pair<Vector2D, Vector2D>> to_add;
			for (auto &m: match) {
				Vector2D p0(m.x0, m.y0), p1(m.x1, m.y1);
				if (within_01(homo0_inv.trans_normalized(homo1.trans(p1))) &&
						within_01(homo1_inv.trans_normalized(homo0.trans(p0)))) {

					to_add.emplace_back(p0, p1);
				}
			}
			if (to_add.size() < match.size())
				log_printf("BA init: rejected %d outliers between %d and %d",
						int(match.size() - to_add.size()), int(j), int(i));
			if (to_add.empty())
				continue;
			real_t f = 1.0 / to_add.size();
			for (auto &t: to_add)
				adjustor.add_term(f, i, t.first, j, t.second);
		}
	}

	size_t id_comp = component.size();
	for (size_t i = 0; i < component.size(); i ++)
		if (component[i].image_idx == composed.identity_image_idx) {
			id_comp = i;
			break;
		}
	assert(id_comp < component.size());
	adjustor.add_homography(id_comp, component[id_comp].homography);
	adjustor.set_preserve(id_comp);

	vector<bool> done(component.size(), false);
	done[id_comp] = true;
	for (; ;) {
		size_t cur_comp = id_comp;
		// find cur_comp that has maximal match
		{
			int max_match = -1;
			for (size_t i = 0; i < component.size(); i ++)
				if (!done[i]) {
					int cur_match = 0;
					auto &pm = m_pairwise_match[component[i].image_idx];
					for (size_t j = 0; j < component.size(); j ++)
						if (done[j]) {
							cur_match +=
								pm[component[j].image_idx].matched.size();
						}
					if (cur_match > max_match) {
						max_match = cur_match;
						cur_comp = i;
					}
				}
			if (cur_comp == id_comp)
				break;
			assert(max_match > 0);
		}
		adjustor.add_homography(cur_comp, component[cur_comp].homography);
		auto stat = adjustor.run();
		adjustor.set_preserve(cur_comp);
		done[cur_comp] = true;
		log_printf("BA %d: err reduced %.2f%% [to %.2e], outlier %d",
				int(component[cur_comp].image_idx),
				(1 - stat.err_after / stat.err_before) * 100,
				stat.err_after, stat.nr_outlier);
	}

	for (size_t i = 0; i < component.size(); i ++)
		adjustor.set_preserve(i, i == id_comp);
	auto stat = adjustor.run();
	log_printf("global BA: err reduced %.2f%% [to %.2e], outlier %d",
			(1 - stat.err_after / stat.err_before) * 100,
			stat.err_after, stat.nr_outlier);

}

void ImageStitcher::Impl::calc_mst() {

	size_t nr_vtx = m_pairwise_match.size();

	m_mst_connected.resize(nr_vtx);
	for (auto &m: m_mst_connected) {
		m.clear();
		m.resize(nr_vtx, 0);
	}

	struct VertexInfo {
		bool used = false;
		real_t error = numeric_limits<real_t>::max();
		int par_vtx,
			comp_idx;	//! index in the MST component
	};
	vector<VertexInfo> graph_node(nr_vtx);

	// MST using the prime algorithm
	auto update_dist = [&graph_node, this, nr_vtx](
			int root) {
		auto &oarc = m_pairwise_match[root];
		for (size_t i = 0; i < nr_vtx; i ++) {
			auto &t = graph_node[i];
			if (!t.used && oarc[i].error < t.error) {
				t.error = oarc[i].error;
				t.par_vtx = root;
			}
		}
	};

	m_composed.clear();
	for (size_t root = 0; root < nr_vtx; root ++) {
		if (graph_node[root].used) 
			continue;

		log_printf("computing MST starting with image #%d",
				int(root));

		graph_node[root].used = true;
		graph_node[root].comp_idx = 0;
		m_composed.push_back(ComposedImage());
		auto &cur_composed = m_composed.back();
		auto &component = cur_composed.component;
		component.emplace_back(root);
		component[0].homography.set_as_identity();
		update_dist(root);
		for (; ; ) {
			int this_vtx = -1;
			real_t min_err = numeric_limits<real_t>::max();
			// find the vertex with minimal error
			{
				for (size_t i = 0; i < nr_vtx; i ++) {
					auto &t = graph_node[i];
					if (!t.used && t.error < min_err) {
						min_err = t.error;
						this_vtx = i;
					}
				}
				if (this_vtx == -1)
					break;
			}
			component.emplace_back(this_vtx);
			auto &this_node = graph_node[this_vtx];
			this_node.comp_idx = component.size() - 1;
			this_node.used = true;

			int par_vtx = this_node.par_vtx;
			ImageComponent &cur_comp = component.back(),
						   &par_comp = component[graph_node[par_vtx].comp_idx];
			auto &cur_homo = cur_comp.homography;
			cur_homo = par_comp.homography *
				m_pairwise_match[this_vtx][par_vtx].trans;

			update_dist(this_vtx);

			log_printf("MST: #%d -> #%d err=%.4e", this_vtx, par_vtx, min_err);

			m_mst_connected[par_vtx][this_vtx] =
				m_mst_connected[this_vtx][par_vtx] = true;
		}

		log_printf("number of images in this MST: %d",
				int(component.size()));

	}
}


void ImageStitcher::Impl::calc_pairwise_match(
		const FeatureMatcherPtrArray &matcher) {

	auto &pm = m_pairwise_match;
	pm.clear();
	pm.resize(matcher.size());
	// pm[i][j].trans: transform from i to j
	// compute pairwise matched points
	for (auto &i: pm)
		i.resize(matcher.size());

#pragma omp parallel for schedule(dynamic)
	for (size_t i = 0; i < matcher.size(); i ++)
		for (size_t j = i + 1; j < matcher.size(); j ++) {

			if (m_stitcher.m_match_method == 1 && j != i + 1 &&
					!(i == 0 && j == matcher.size() - 1))
				continue;

			auto &tgt = pm[i][j];
			calc_homography(
					find_pairwise_feature_match(*matcher[i], *matcher[j]),
					tgt);
			if (tgt.matched.empty())
				continue;

			pm[j][i].error = tgt.error;
			pm[j][i].matched.clear();
			for (auto &m: tgt.matched)
				pm[j][i].matched.emplace_back(m.x1, m.y1, m.x0, m.y0);
			pm[j][i].trans = tgt.trans.get_inv();

			log_printf("%d <-> %d: %d points, err %.3e",
					int(i), int(j), int(tgt.matched.size()), tgt.error);

			if (m_stitcher.m_draw_match)
				draw_match(tgt.matched, tgt.trans, i, j);
		}
}

MatchedPointArray find_pairwise_feature_match(
		const FeatureMatcherBase &matcher0,
		const FeatureMatcherBase &matcher1) {

	MatchedPointArray result;

	for (auto &f: matcher0.get_feature_set()) {
		const FeatureKeypoint *f1 = matcher1.match(f), *f0;
		if (f1 && (f0 = matcher0.match(*f1)) != nullptr 
				&& f0->id == f.id)
			result.emplace_back(f0->r, f0->c, f1->r, f1->c);
	}

	sort(result.begin(), result.end(),
			[](const MatchedPoint &p0, const MatchedPoint &p1) -> bool {
				#define CMP(c) \
					if (fabs(p0.c - p1.c) > EPS) \
						return p0.c < p1.c
				CMP(x0); CMP(x1); CMP(y0); CMP(y1);
				return false;
				#undef CMP
			});
	auto uniq_end = unique(result.begin(), result.end(),
			[](const MatchedPoint &p0, const MatchedPoint &p1) -> bool {
				#define CMP(c) \
					(fabs(p0.c - p1.c) < EPS)
				return CMP(x0) && CMP(x1) && CMP(y0) && CMP(y1);
				#undef CMP
			});
	result.erase(uniq_end, result.end());

	return result;
}

void ImageStitcher::Impl::calc_homography(
		const MatchedPointArray &dataset, PairwiseMatchInfo &match_info) const {

	size_t good_match_threshold = m_stitcher.m_fit_good_nr_match;
	if (dataset.size() < good_match_threshold)
		return;

	MatchedPointArray dataset_shuffled(dataset);

	real_t err_threshold_sqr = sqr(m_stitcher.m_fit_error_threshold),
		   malform_threshold = m_stitcher.m_homocoord_threshold;

	int nr_malform = 0,
		nr_iter = m_stitcher.m_fit_nr_iter;

	match_info.error = numeric_limits<real_t>::max();

	mt19937 random_engine;

	for (int i = 0; i < nr_iter; i ++) {
		for (int j = 0; j < 4; j ++) {
			real_t rand = random_engine() / (random_engine.max() + 1.0);
			swap(dataset_shuffled[j],
				dataset_shuffled[j + ifloor(
					(dataset_shuffled.size() - j) * rand)]);
		}

		Homography cur_model(Homography::calc_from_match(
					MatchedPointArray(
						dataset_shuffled.begin(),
						dataset_shuffled.begin() + 4)));

		if (cur_model.min_w() < malform_threshold) {
			/*
			 * [x1 y1 d1]' = H * [x y 1]'
			 * x' = x1 / d1, y' = y1 / d1
			 * must have d1 not cross origin (i.e always > 0)
			 */
			nr_malform ++;
			continue;
		}

		MatchedPointArray consensus_set;
		for (auto &m: dataset) {
			Vector2D t(cur_model.trans_normalized(m.x0, m.y0));
			if (!t.is_nan()) {
				real_t err = quadratic_sum(m.x1 - t.x, m.y1 - t.y);
				if (err < err_threshold_sqr)
					consensus_set.push_back(m);
			}
		}

		if (consensus_set.size() >= good_match_threshold) {
			cur_model = Homography::calc_from_match(consensus_set);
			real_t cur_err = 0;
			bool failed = false;
			for (auto &m: consensus_set) {
				Vector2D t(cur_model.trans_normalized(m.x0, m.y0));
				if (t.is_nan()) {
					failed = true;
					break;
				}
				real_t err = quadratic_sum(m.x1 - t.x, m.y1 - t.y);
				cur_err += err;
			}
			if (failed)
				continue;
			cur_err = sqrt(cur_err) / consensus_set.size();
			if (cur_err < match_info.error) {
				match_info.error = cur_err;
				match_info.matched = consensus_set;
				match_info.trans = cur_model;
			}
		}
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
