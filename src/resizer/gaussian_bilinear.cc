/*
 * $File: gaussian_bilinear.cc
 * $Date: Wed May 01 14:33:50 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "resizer/bilinear.hh"
#include "resizer/gaussian_bilinear.hh"
#include "filter/gaussian.hh"

Image GaussianBilinearResizer::resize(const Image &img_in, real_t factor) {
	/*
	 * sample wavelength: 1/factor
	 * sample frequency: factor
	 * factor >= 2*actual_frequency
	 * k/gaussian_sigma = actual_frequency <= factor / 2
	 * gaussian_sigma >= 2 * k / factor
	 */
	BilinearResizer bilinear;
	bilinear.use_half_downsample = false;
	if (factor > 1)
		return bilinear.resize(img_in, factor);
	GaussianFilter filter(0.4 / factor);
	return bilinear.resize(filter.run(img_in), factor);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

