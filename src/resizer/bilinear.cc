/*
 * $File: bilinear.cc
 * $Date: Wed May 01 14:33:28 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "resizer/bilinear.hh"
#include "utils.hh"
#include "math.hh"


Image BilinearResizer::resize(const Image &img_in_orig, real_t factor) {
	Image img_in = img_in_orig;

	if (fabs(factor - 1) < EPS)
		return img_in.copy();

	if (use_half_downsample && factor <= 0.5 + EPS) {
		Image tmp[2];
		tmp[0] = img_in.copy();
		int cur = 0;
		while (factor <= 0.5 + EPS) {
			downsample_half(tmp[cur], tmp[cur ^ 1]);
			cur ^= 1;
			factor *= 2;
		}
		img_in = tmp[cur];
		if (fabs(factor - 1) < EPS)
			return img_in;
	}

	int width = img_in.width(), height = img_in.height(),
		dwidth = int(width * factor), dheight = int(height * factor);

	assert(dwidth >= 1 && dheight >= 1);

	Image ret = Image::create(dwidth, dheight);
	pixel_t *dest = ret.data();

	factor = 1.0 / factor;
	for (int i = 0; i < dheight; i ++) {
		real_t r = i * factor;
		int ri = ifloor(r);
		r -= ri;
		if (ri == height - 1)
			ri --;
		const pixel_t *src = img_in.data(ri, 0);

		for (int j = 0; j < dwidth; j ++) {
			real_t c = j * factor;
			int ci = ifloor(c);
			c -= ci;
			if (ci == width - 1)
				ci --;
			const pixel_t register *cur = src + ci;
			*(dest ++) = cur[0] * (1 - r) * (1 - c) + cur[1] * (1 - r) * c +
				cur[width] * r * (1 - c) + cur[width + 1] * r * c;
		}
	}

	return ret;
}

void BilinearResizer::downsample_half(const Image &img_in, Image &img_out) {
	int sw = img_in.width(), dw = sw / 2, dh = img_in.height() / 2;
	img_out.set_dimension(dw, dh);
	pixel_t *dest = img_out.data();
	for (int i = 0; i < dh; i ++) {
		const pixel_t *src = img_in.data(i * 2, 0);
		for (int j = 0; j < dw; j ++) {
			*(dest ++) = 0.25 * (src[0] + src[1] + src[sw] + src[sw + 1]);
			src += 2;
		}
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

