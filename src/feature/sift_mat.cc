/*
 * $File: sift_mat.cc
 * $Date: Tue Apr 09 10:11:54 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \file
 * \brief put the implementation of function invoving matrix manipulation here
 *			to speed up compile
 */

#include "sift_impl.hh"

#include <Eigen/Dense>


bool SIFTFeature::Impl::adjust_local_extrema(
		real_t &row_precise, real_t &col_precise, int &scale) {

	real_t edge_threshold = m_detector.m_edge_threshold,
		edge_threshold_p1sqr = (edge_threshold + 1) * (edge_threshold + 1);
	int border_size = m_detector.m_border_size;
	int width = m_img_dog[0].width(), height = m_img_dog[0].height();

	for (int i = 0; i < m_detector.m_subpxlrefine_nr_iter; i ++) {
		int row = int(round(row_precise)), col = int(round(col_precise));
		if (row < border_size || row >= height - border_size ||
				col < border_size || col >= width - border_size ||
				scale <= 0 || scale + 1 >= int(m_img_dog.size()))
			return false;

		const pixel_t
			*cur = m_img_dog[scale].data(row, col),
			*prev = m_img_dog[scale - 1].data(row, col),
			*next = m_img_dog[scale + 1].data(row, col);


		real_t dxx = cur[width] + cur[-width] - cur[0] * 2,
			   dxy = (cur[width + 1] + cur[-width - 1]
					   - cur[width - 1] - cur[-width + 1]) * 0.25,
			   dxs = (prev[-width] + next[width] - prev[width] - next[-width]) * 0.25,
			   dyy = cur[1] + cur[-1] - cur[0] * 2,
			   dys = (prev[-1] + next[1] - prev[1] - next[-1]) * 0.25,
			   dss = (next[0] + prev[0] - cur[0] * 2);

		Eigen::Matrix3f hessian;
		hessian <<
			dxx, dxy, dxs,
			dxy, dyy, dys,
			dxs, dys, dss;
		Eigen::Vector3f b;
		b <<
			(cur[width] - cur[-width]) * 0.5,
			(cur[1] - cur[-1]) * 0.5,
			(next[0] - prev[0]) * 0.5;
		
		Eigen::Vector3f x = hessian.colPivHouseholderQr().solve(b);
		x = -x;

		row_precise += x[0];
		col_precise += x[1];
		if (fabs(row_precise - row) <= 0.5 && fabs(col_precise - col) <= 0.5
				&& fabs(x[2]) < 0.5) {
			real_t det = dxx * dyy - dxy * dxy, tr = dxx + dyy;
			if (det < 0 ||
					tr * tr * edge_threshold >= edge_threshold_p1sqr * det)
				return false;

			return true;
		}

		scale = int(round(scale + x[2]));
	}

	m_stat.adjust_no_converge ++;
	return false;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
