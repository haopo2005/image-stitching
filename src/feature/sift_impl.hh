/*
 * $File: sift_impl.hh
 * $Date: Sun Apr 28 17:11:44 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

/*!
 * \file
 * \brief internal implementation for SIFT
 */

#include "feature/sift.hh"
#include "filter/gaussian.hh"

#include <cmath>
#include <cstring>

#include <vector>
#include <memory>
using namespace std;

using descriptor_t = shared_ptr<real_t>;
struct SIFTKeypoint {
	real_t r, c;	//! ranges from 0 to 1
	real_t orientation;	/*! ranges from 0 to 2pi, the angle respect to positive
						  r axis (i.e x axis) */
	descriptor_t descriptor;


	SIFTKeypoint(real_t r_, real_t c_, real_t ori, const descriptor_t &desc):
		r(r_), c(c_), orientation(ori), descriptor(desc)
	{}
};

class SIFTFeature::Impl {
	const SIFTFeature &m_detector;
	const Image &m_image;

	/*!
	 * \brief statistics
	 */
	struct Stat {
		int extrema,	//! number of extrema keypoints
			remove_ale,	/*! number of keypoints removed by adjust_local_extrema,
							including adjust_no_converge */
			remove_ctr, //! number of keypoints removed due to low contrast
			adjust_no_converge; /*! number of keypoints not converged during
								  adjust_local_extrema */
		void reset() {
			memset(this, 0, sizeof(*this));
		}
	};
	Stat m_stat;

	vector<GaussianFilter> m_filters; //! create layers in each octave

	vector<SIFTKeypoint> m_keypoint;

	vector<Image> m_img_gauss,	/*! image after applying GaussianFilter of
								  different scales in one octave */
				  m_img_dog;	/*! difference of Gaussian, in one octave;
								  m_img_dog[i] = m_img_gauss[i + 1] -
								  m_img_gauss[i] */

	int m_descr_size;	//! number of components in the descriptor vector
	real_t m_add_keypoint_hist_mem[365];

	/*!
	 * \brief initialize m_filters so that they can be used to create Gaussian
	 *			layers
	 */
	void init_filter();

	/*!
	 * \brief search for local extrema in the scale space, store result in
	 *		m_keypoint
	 */
	void search_local_extrema();

	/*!
	 * \brief build DOG layers (i.e. init m_img_gauss and m_img_dog)
	 */
	void build_dog_layers(const Image &base_img);

	/*!
	 * \brief search for local extrema in one octave
	 */
	void search_local_extrema_in_cur_octave(const Image &img);

	/*!
	 * \brief calc orientation and descriptor, and add keypoint to m_keypoint
	 */
	void add_keypoint(const Image &img, real_t row, real_t col, int radius,
			real_t sigma);

	/*!
	 * \brief adjust local extrema coordinate using quadratic interpolation
	 * Note that also check edge response, and reject the point accordingly
	 */
	bool adjust_local_extrema(real_t &row, real_t &col, int &scale);

	/*!
	 * \brief compute the descriptor at a keypoint
	 */
	descriptor_t calc_descriptor(const Image &img,
			int row, int col, real_t orientation);

	public:
		Impl(const SIFTFeature &detector, const Image &image):
			m_detector(detector), m_image(image)
		{}

		vector<Point> detect();

		Image draw_keypoint();
		vector<FeatureKeypoint> extract_feature();
};


// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
