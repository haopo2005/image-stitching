/*
 * $File: stitcher.hh
 * $Date: Tue Apr 30 23:33:03 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "feature/base.hh"

#include <vector>

class ImageStitcher {
	class Impl;

	bool m_init_done = false;
	Impl *m_impl;

	ImageStitcher(const ImageStitcher &) = delete;
	ImageStitcher& operator = (const ImageStitcher &) = delete;

	public:
		using FeatureMatcherPtrArray = std::vector<FeatureMatcherBase*>;
		using ImageArray = std::vector<Image>;

		bool m_draw_match = false,	/*! whether to draw matched points for debug
									  or demonstration */
			 m_dump_homo_point = false,	/*! whether to dump transformed
										  homogeneous coordinate to obj file */
			 m_save_transformed_img = false;

		int m_fit_nr_iter,
			m_fit_good_nr_match,	/*! minimal number of matched points to be
									  considered a good match */
			m_seam_width,	/*! to make the pixels in the neighbor of the seam
								change from img1 to img0 */
			m_match_method,	/*! method for matching images; 
							  0 - try to perform pairwise match
							  1 - only match adjacent images */
			m_blend_method,	/*! blending method:
							  0 - linear
							  1 - multiband */
			m_output_image_max_area,
			m_output_image_max_size;

		real_t m_fit_error_threshold,	/*! count as correct fit if error not
										  exceeding this */
			   m_homocoord_threshold,	/*! minimal value of third component of
										  the transformed homogeneous coordinate
										  (used for identifying malformed
										  homography) */
			   m_fit_sample_size_ratio,	//! see m_fit_sample_size_min
			   m_proj_flat_minw,		/*! if w values of all projection points
										  are above this value, use flat
										  projector*/
			   m_proj_flat_max_angle,	/*! if angles between normals of
										  transformed images below this value,
										  use flat projector */
			   m_proj_cylin_min_agl,	/*! minimal angle required to use
										  cylindrical projection */
			   m_ba_conf_thresh;		/*! distance threshold to add matched
										  points to BA*/

		ImageStitcher();
		~ImageStitcher();

		/*!
		 * \brief prepare for stitching
		 * \param matcher provides information about features; must have been
		 *		properly initialized
		 * \param feature_image images providing features 
		 */
		void init(const FeatureMatcherPtrArray &matcher,
				const ImageArray &feature_image);

		/*!
		 * \brief perform the stitching; must call init first
		 * \param image the images corresponding to features stored in
		 *		_matcher_ passed to to _init_
		 * \return all the composed images
		 */
		ImageArray stitch(const ImageArray &image) const;
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

