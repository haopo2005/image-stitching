/*
 * $File: multiband.hh
 * $Date: Wed Apr 24 16:50:05 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "base.hh"

class MultibandBlender: public BlenderBase {
	class Impl;
	Impl *m_impl;

	MultibandBlender(const MultibandBlender&) = delete;
	MultibandBlender& operator = (const MultibandBlender&) = delete;

	public:
		bool m_show_band = false;
		int m_nr_band;	//! number of frequency bands to be used
		real_t m_sigma_step; //! wavelength of each frequency band

		MultibandBlender();
		~MultibandBlender();

		void add_image(const Point &upper_left,
				const Matrix<Vector2D> &orig_pos, const Image &img);

		void run(Image &target);
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

