/*
 * $File: base.hh
 * $Date: Sun Apr 28 17:48:10 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "image.hh"
#include "vector.hh"

class BlenderBase {
	public:
		virtual ~BlenderBase() {}

		/*!
		 * \brief add an image to be blended
		 * \param upper_left position of upper left corner of this image on
		 *		target
		 * \param orig_pos maps each pixel on the target image to scaled
		 *		coordinate on original image, or NaN if not on target image
		 * \param img the original image
		 */
		virtual void add_image(const Point &upper_left,
				const Matrix<Vector2D> &orig_pos, const Image &img) = 0;

		virtual void run(Image &target) = 0;
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

