/*
 * $File: vector.hh
 * $Date: Thu May 02 14:27:23 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "math.hh"
#include <limits>

class Vector2D {
	static constexpr real_t NAN_VAL = std::numeric_limits<real_t>::max();
	
	public:
		real_t x, y;

		explicit Vector2D(real_t x_ = 0, real_t y_ = 0):
			x(x_), y(y_)
		{}

		/*!
		 * \brief from p0 to p1
		 */
		Vector2D(const Vector2D &p0, const Vector2D &p1):
			x(p1.x - p0.x), y(p1.y -p0.y)
		{}

		real_t cross(const Vector2D &v) const
		{ return x * v.y - y * v.x; }

		real_t dot(const Vector2D &v) const
		{ return x * v.x + y * v.y; }

		Vector2D operator + (const Vector2D &v) const
		{ return Vector2D(x + v.x, y + v.y); }

		Vector2D& operator += (const Vector2D &v)
		{ x += v.x; y += v.y; return *this; }

		Vector2D operator - (const Vector2D &v) const
		{ return Vector2D(x - v.x, y - v.y); }

		Vector2D& operator -= (const Vector2D &v)
		{ x -= v.x; y -= v.y; return *this; }

		Vector2D operator * (real_t f) const
		{ return Vector2D(x * f, y * f); }

		real_t mod_sqr() const
		{ return x * x + y * y; }

		real_t mod() const
		{ return sqrt(mod_sqr()); }

		void set_zero() 
		{ x = y = 0; }

		bool in_triangle(Vector2D p0, Vector2D p1, Vector2D p2) const {
			p0 -= *this;
			p1 -= *this;
			p2 -= *this;
			int a = get_sign(p0.cross(p1)),
				b = get_sign(p1.cross(p2)),
				c = get_sign(p2.cross(p0));

			return (a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0);
		}

		static Vector2D NaN() {
			return Vector2D(NAN_VAL, NAN_VAL);
		}

		bool is_nan() const {
			return x == NAN_VAL && y == NAN_VAL;
		}
};

class Vector3D {

	public:
		real_t x, y, z;

		explicit Vector3D(real_t x_ = 0, real_t y_ = 0, real_t z_ = 0):
			x(x_), y(y_), z(z_)
		{}

		Vector3D(const Vector3D &p0, const Vector3D &p1):
			x(p1.x - p0.x), y(p1.y -p0.y), z(p1.z - p0.z)
		{}

		/*!
		 * \brief get component by index (0 for x, 1 for y, 2 for z)
		 */
		real_t operator [](int idx) const {
			assert(idx >= 0 && idx < 3);
			return (&x)[idx];
		}

		real_t mod_sqr() const
		{ return x * x + y * y + z * z; }

		real_t mod() const
		{ return sqrt(mod_sqr()); }

		real_t dot(const Vector3D &v) const
		{ return x * v.x + y * v.y + z * v.z; }

		Vector3D cross(const Vector3D &v) const {
			return
				Vector3D(y * v.z - z * v.y,
						z * v.x - x * v.z, x * v.y - y * v.x);
		}


		Vector3D operator + (const Vector3D &v) const
		{ return Vector3D(x + v.x, y + v.y, z + v.z); }

		Vector3D& operator += (const Vector3D &v)
		{ x += v.x; y += v.y; z += v.z; return *this; }

		Vector3D operator - (const Vector3D &v) const
		{ return Vector3D(x - v.x, y - v.y, z - v.z); }

		Vector3D operator - () const
		{ return Vector3D(-x, -y, -z); }

		Vector3D& operator -= (const Vector3D &v)
		{ x -= v.x; y -= v.y; z -= v.z; return *this; }


		Vector3D operator * (real_t f) const
		{ return Vector3D(x * f, y * f, z * f); }

		Vector3D& operator *= (real_t f)
		{ x *= f; y *= f; z *= f; return *this; }

		bool operator == (const Vector3D &v) const
		{ return fabs(x - v.x) < EPS && fabs(y - v.y) < EPS && fabs(z - v.z) < EPS; }

		void normalize() {
			real_t f = 1.0 / mod();
			assert(isnormalr(f));
			x *= f; y *= f; z *= f;
		}

		Vector3D get_normalized() const {
			real_t f = 1.0 / mod();
			assert(isnormalr(f));
			return Vector3D(x * f, y * f, z * f);
		}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

