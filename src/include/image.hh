/*
 * $File: image.hh
 * $Date: Mon Apr 22 11:21:03 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <memory>

#include "math.hh"

/*!
 * \file
 * \brief definitions of basic types/structures for image manipulation
 */

typedef float pixel_t;

/*!
 * \brief represent a point in an image
 */
struct Point {
	int r, c;
	// x axis is r, y axis is c
	
	Point(int r_ = -1, int c_ = -1):
		r(r_), c(c_)
	{}

	bool operator == (const Point &p) const {
		return r == p.r && c == p.c;
	}
};

class Image: public Matrix<pixel_t> {
	static Image m_black;

	public:	
		Image() {}

		Image(int w, int h, std::shared_ptr<pixel_t> data):
			Matrix<pixel_t>(w, h, data)
		{}

		/*!
		 * \brief a special black image
		 */
		static const Image& black()
		{ return m_black; }

		/*!
		 * \brief whether this image is the black image
		 */
		bool is_black() const
		{ return this == &m_black; }

		Image& set_dimension(int width, int height) {
			Matrix<pixel_t>::set_dimension(width, height);
			return *this;
		}

		Image& set_dimension(const Image &img) {
			return set_dimension(img.m_width, img.m_height);
		}

		/*!
		 * \brief allocate a new buffer filled with 0 for this image
		 */
		Image& reset(int width, int height) {
			Matrix<pixel_t>::reset(width, height);
			return *this;
		}

		/*!
		 * \brief display this image on screen
		 */
		void display() const;

		/*!
		 * \brief save the image to file
		 * pass a printf-like format string
		 */
		void save(const char *fpath_fmt, ...) const
			__attribute__((format(printf, 2, 3)));

		/*!
		 * \brief return a copy of this image
		 */
		Image copy() const;

		/*!
		 * \brief calculate the difference of two images
		 */
		Image operator - (const Image &img) const;

		/*!
		 * \brief assign value of this image to the difference of the two images
		 */
		Image& assign_as_difference(const Image &a, const Image &b);

		/*!
		 * \brief linearly scale all pixels so that min = 0 and max = 1
		 */
		Image& linear_normalize();

		/*!
		 * \brief perform gamma correction on this image
		 */
		Image& gamma_correction(real_t gamma);

		/*!
		 * \brief create a new image initialized as black
		 */
		static Image create(int width, int height);

		inline pixel_t get_linear_interpolation(real_t xr, real_t yr) const {
			int xi = ifloor(xr), yi = ifloor(yr);
			assert(xi >= 0 && xi < m_height);
			assert(yi >= 0 && yi < m_width);
			xr -= xi;
			yr -= yi;
			if (xi == m_height - 1)
				xi --;
			if (yi == m_width - 1)
				yi --;
			const pixel_t *src = m_data.get() + xi * m_width + yi;
			return src[0] * (1 - xr) * (1 - yr) + src[1] * (1 - xr) * yr +
				src[m_width] * xr * (1 - yr) + src[m_width + 1] * xr * yr;
		}

		/*!
		 * \brief get linear interpolated value at scaled coordinate
		 */
		inline pixel_t get_linear_interpolation_sc(real_t x, real_t y) const {
			return get_linear_interpolation(x * m_height, y * m_width);
		}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

