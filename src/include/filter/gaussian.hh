/*
 * $File: gaussian.hh
 * $Date: Wed Apr 10 23:19:07 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include "base.hh"
#include <memory>

/*!
 * \brief Gaussian blur filter
 */
class GaussianFilter: public FilterBase {
	int m_size;	//! half size of kernel (center to edge)
	std::shared_ptr<real_t> m_weight_buf;
	real_t *m_weight;	//! index range: [-m_size, m_size]
	real_t m_sigma;

	public:
		using FilterBase::run;

		GaussianFilter(real_t sigma);

		void run(const Image &img_in, pixel_t *img_out);

		real_t sigma() const {
			return m_sigma;
		}
};

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

