/*
 * $File: utils.hh
 * $Date: Tue Apr 30 23:43:50 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <functional>
#include <memory>
#include <cstring>

/*!
 * \brief register a function to be called for initialization
 */
#define REGISTER_INIT_FUNC(func) \
	static InitRegister _INIT_FUNC_NAME(__COUNTER__) (func)

#define __INIT_FUNC_NAME(x) __init_func_ ## x
#define _INIT_FUNC_NAME(x) __INIT_FUNC_NAME(x)

/*!
 * \brief register init function
 * usually this class not directly used; use REGISTER_INIT_FUNC instead
 */
class InitRegister {
	public:
		typedef std::function<void()> func_t;

		static void add_func(func_t func);
		static void init();

		InitRegister(func_t func) {
			add_func(func);
		}
};

#define assert(expr) \
	do { \
		if (!(expr)) \
			__assert_failed__(__FILE__, __func__, \
					__LINE__, # expr); \
	} while (0)


#define log_printf(fmt, ...) \
	__log_printf__(__FILE__, __func__, __LINE__, fmt, ## __VA_ARGS__)


double get_cputime();

class Timer {
	double m_start_time;
	std::function<void(Timer &t)> m_on_destroy;

	public:
		Timer(std::function<void(Timer &t)> on_destroy = [](Timer &t){}):
			m_start_time(get_cputime()),
			m_on_destroy(on_destroy)
		{}

		~Timer() {
			m_on_destroy(*this);
		}

		double get_time() const {
			return get_cputime() - m_start_time;
		}

		void reset() {
			m_start_time = get_cputime();
		}
};

template <typename T>
std::shared_ptr<T> create_auto_buf(size_t len, bool init_zero = false) {
	std::shared_ptr<T> ret(new T[len], [](T *ptr){delete []ptr;});
	if (init_zero)
		memset(ret.get(), 0, sizeof(T) * len);
	return ret;
}

/*!
 * \brief peak virtual memory usage
 */
size_t get_peak_vm();

/*!
 * \brief ask user to confirm if file exists
 */
bool confirm_overwrite(const char *fpath);

// internal implementations, ignore
void __assert_failed__(const char *file, const char *func, int line,
		const char *expr);

void __log_printf__(const char *file, const char *func, int line,
		const char *fmt, ...) __attribute__((format(printf, 4, 5)));

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

