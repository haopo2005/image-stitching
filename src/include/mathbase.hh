/*
 * $File: mathbase.hh
 * $Date: Thu May 02 15:22:13 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#pragma once

#include <limits>
#include <cmath>
typedef float real_t;

constexpr real_t EPS = std::numeric_limits<real_t>::epsilon() * 2;

static inline bool isnormalr(real_t x) {
	auto c = std::fpclassify(x);
	return c == FP_ZERO || c == FP_NORMAL;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

