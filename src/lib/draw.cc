/*
 * $File: draw.cc
 * $Date: Mon Apr 08 08:12:27 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "draw.hh"
#include "utils.hh"

#include <cmath>

#include <algorithm>
using namespace std;

void draw::point(Image &img, const Point &pos, pixel_t color) {
	int width = img.width(), height = img.height();
	assert(pos.r >= 0 && pos.r < height);
	assert(pos.c >= 0 && pos.c < width);

	pixel_t *data = img.data(pos.r, pos.c);
	data[0] = color;
	for (int dr: {-1, 1})
		for (int dc: {-1, 1}) {
			int r = pos.r + dr, c = pos.c + dc, delta = dr * width + dc;
			pixel_t *t = data;
			for (int i = 0; i < 5 && r >= 0 && r < height
					&& c >= 0 && c < width; i ++) {
				t += delta;
				*t = color;
				r += dr;
				c += dc;
			}
		}
}

void draw::circle(Image &img, const Point &center, int radius,
		pixel_t color) {
	// http://en.wikipedia.org/wiki/Midpoint_circle_algorithm

	int err = -radius, x = radius, y = 0,
		width = img.width(), height = img.height();
	while (x >= y) {
#define DRAW(dx, dy) \
		do { \
			int r = center.r + (dx), c = center.c + (dy); \
			if (r >= 0 && r < height && c >= 0 && c < width) \
				img.at(r, c) = color; \
		} while(0)

		DRAW(x, y); DRAW(x, -y);
		DRAW(-x, y); DRAW(-x, -y);
		DRAW(y, x); DRAW(y, -x);
		DRAW(-y, x); DRAW(-y, -x);
#undef DRAW

		err += y;
		y ++;
		err += y;
		if (err >= 0) {
			err -= x;
			x --;
			err -= x;
		}
	}
}

void draw::line(Image &img, const Point &p0, const Point &p1,
		pixel_t color) {
	// Bresenham's line algorithm

	int dc = abs(p1.c - p0.c), dr = abs(p1.r - p0.r),
		sc = p0.c < p1.c ? 1 : -1, sr = p0.r < p1.r ? 1 : -1;
	int e = dc - dr,
		c = p0.c, r = p0.r;
	
	for (; ;) {
		img.at(r, c) = color;
		if (c == p1.c && r == p1.r)
			break;
		int e2 = e << 1;
		if (e2 > -dr) {
			e -= dr;
			c += sc;
		}
		if (e2 < dc) {
			e += dc;
			r += sr;
		}
	}
}


void draw::vector(Image &img, const Point &p0, const Point &p1,
			pixel_t color) {

	circle(img, p0, 3, color);
	line(img, p0, p1, color);
}

void draw::image(Image &dest, const Point &pos, const Image &src) {
	int dw = dest.width(), dh = dest.height(),
		sw = src.width(), sh = src.height();
	assert(pos.r + sh <= dh && pos.c + sw <= dw);

	for (int i = 0; i < sh; i ++)
		memcpy(dest.data(i + pos.r, pos.c), src.data(i, 0),
				sizeof(pixel_t) * sw);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

