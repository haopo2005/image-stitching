/*
 * $File: equalizer.cc
 * $Date: Wed May 01 00:18:46 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"
#include "config.hh"
#include "filter/equalizer.hh"
#include "exc.hh"

#include <vector>
#include <algorithm>
using namespace std;

class TrainableEqualizerFilter::Impl {

	vector<pixel_t> m_src, m_dest;
	TrainableEqualizerFilter &m_equalizer;
	shared_ptr<pixel_t> m_map;

	shared_ptr<real_t> calc_cdf(const vector<pixel_t> &samples) const;

	void calc_map();

	public:
		Impl(TrainableEqualizerFilter &equalizer):
			m_equalizer(equalizer)
		{}

		void add(pixel_t src, pixel_t dest) {
			m_src.push_back(src);
			m_dest.push_back(dest);
		}
		
		void finish_add() {
			calc_map();
		}

		void run_modify(Image &img);

		void run(const Image &img_in, pixel_t *img_out);
};

void TrainableEqualizerFilter::Impl::run(
		const Image &img_in, pixel_t *img_out) {
	pixel_t* map = m_map.get();
	if (!map)
		throw IstitcherError("attempt to call run without finish_add()");

	int nquantum = m_equalizer.m_nr_quantum;
	const pixel_t *src = img_in.data();
	for (int i = img_in.width() * img_in.height(); i --; ) {
		real_t s = *(src ++);
		if (s < 0)
			s = 0;
		else if (s > 1)
			s = 1;
		*(img_out ++) = map[ifloor(s * nquantum)];
	}
}

void TrainableEqualizerFilter::Impl::run_modify(Image &img) {
	pixel_t* map = m_map.get();
	if (!map)
		throw IstitcherError("attempt to call run without finish_add()");

	int nquantum = m_equalizer.m_nr_quantum;
	for (auto &t: img) {
		if (t < 0)
			t = 0;
		else if (t > 1)
			t = 1;
		t = map[ifloor(t * nquantum)];
	}
}

shared_ptr<real_t> TrainableEqualizerFilter::Impl::calc_cdf(
		const vector<pixel_t> &samples) const {
	int nquantum = m_equalizer.m_nr_quantum;
	shared_ptr<real_t> ret = create_auto_buf<real_t>(nquantum + 1, true);
	real_t *cdf = ret.get();
	for (auto p: samples) {
		if (p < 0)
			p = 0;
		if (p > 1)
			p = 1;
		cdf[ifloor(p * nquantum)] ++;
	}
	
	// compute sum
	for (int i = 1; i <= nquantum; i ++)
		cdf[i] += cdf[i - 1];

	cdf[nquantum] += 1;

	// linear interpolation
	for (int i = 0; i < nquantum; ) {
		int j = i + 1;
		real_t c0 = cdf[i];
		while (j <= nquantum && cdf[j] == c0)
			j ++;
		real_t coeff = (cdf[j] - c0) / (j - i);
		for (int k = i + 1; k < j; k ++)
			cdf[k] = c0 + (k - i) * coeff;
		i = j;
	}

	return ret;
}

void TrainableEqualizerFilter::Impl::calc_map() {
	auto cdf_src_buf = calc_cdf(m_src), cdf_dest_buf = calc_cdf(m_dest);
	real_t *cdf_src = cdf_src_buf.get(), *cdf_dest = cdf_dest_buf.get();
	int nquantum = m_equalizer.m_nr_quantum;
	assert(cdf_src[nquantum] == cdf_dest[nquantum]);
	m_map = create_auto_buf<pixel_t>(nquantum + 1);
	pixel_t *map = m_map.get();
	for (int i = 0, t = 0; i <= nquantum; i ++) {
		while (cdf_dest[t] < cdf_src[i])
			t ++;
		map[i] = real_t(t) / nquantum;
	}
}

TrainableEqualizerFilter::TrainableEqualizerFilter():
	m_impl(new Impl(*this)),
	m_nr_quantum(config.get_int("EQUALIZER.NR_QUANTUM", 255))
{
}

TrainableEqualizerFilter::~TrainableEqualizerFilter() {
	delete m_impl;
}

void TrainableEqualizerFilter::add(pixel_t src, pixel_t dest) {
	m_impl->add(src, dest);
}

void TrainableEqualizerFilter::run(const Image &img_in, pixel_t *img_out) {
	m_impl->run(img_in, img_out);
}

void TrainableEqualizerFilter::finish_add() {
	m_impl->finish_add();
}

void TrainableEqualizerFilter::run_modify(Image &img) {
	m_impl->run_modify(img);
}


// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

