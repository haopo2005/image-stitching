/*
 * $File: linear.cc
 * $Date: Sun Apr 28 19:46:28 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "blender/linear.hh"


void LinearBlender::add_image(const Point &upper_left,
		const Matrix<Vector2D> &orig_pos, const Image &img) {

	Matrix<WeightedPixel> wimg;
	int width = orig_pos.width(), height = orig_pos.height();
	wimg.reset(width, height);
	for (int i = 0; i < height; i ++)
		for (int j = 0; j < width; j ++) {
			auto &p = orig_pos.at(i, j);
			auto &t = wimg.at(i, j);
			if (!p.is_nan()) {
				t.intensity = img.get_linear_interpolation_sc(p.x, p.y);
				t.weight = (0.5 - fabs(p.x - 0.5)) * 
					(0.5 - fabs(p.y - 0.5));
			}
		}
	m_image.emplace_back(wimg,
			Range2D(upper_left, width, height));
}

void LinearBlender::run(Image &target) {
	for (int i = 0; i < target.height(); i ++) {
		pixel_t *dest = target.data(i, 0);
		for (int j = 0; j < target.width(); j ++) {
			real_t isum = 0, wsum = 0;
			for (auto &img: m_image)
				if (img.range.contain(i, j)) {
					auto &w = img.img.at(i - img.range.xmin,
							j - img.range.ymin);
					isum += w.weight * w.intensity;
					wsum += w.weight;
				}
			if (wsum)
				dest[j] = isum / wsum;
		}
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}
