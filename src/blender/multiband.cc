/*
 * $File: multiband.cc
 * $Date: Mon Apr 29 17:11:02 2013 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"
#include "utils.hh"
#include "filter/fast_gaussian.hh"
#include "blender/multiband.hh"
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;

namespace multibandblender_impl {
	struct Range2D {
		int xmin, xmax, ymin, ymax;
		Range2D(const Point &upper_left, int width, int height):
			xmin(upper_left.r), xmax(xmin + height),
			ymin(upper_left.c), ymax(ymin + width)
		{}

		inline bool contain(int x, int y) const {
			return x >= xmin && x < xmax && y >= ymin && y < ymax;
		}
	};
	struct WeightedPixel {
		pixel_t intensity;
		real_t weight;	/*! initial weight depends on position: maximal at image
						  center and minimal on border, always > 0; = 0 if this
						  pixel not in original image */

		WeightedPixel& operator += (const WeightedPixel &w) {
			intensity += w.intensity;
			weight += w.weight;
			return *this;
		}

		WeightedPixel& operator -= (const WeightedPixel &w) {
			intensity -= w.intensity;
			weight -= w.weight;
			return *this;
		}

		WeightedPixel operator + (const WeightedPixel &w) const {
			return WeightedPixel(intensity + w.intensity, weight + w.weight);
		}

		WeightedPixel operator * (real_t f) const {
			return WeightedPixel(intensity * f, weight * f);
		}

		WeightedPixel(pixel_t i = 0, real_t w = 0):
			intensity(i), weight(w)
		{}
	};
	using WeightedImage = Matrix<WeightedPixel>;

	struct SourceImage {
		WeightedImage img;
		Range2D range;
		SourceImage(const WeightedImage &i, const Range2D &r):
			img(i), range(r)
		{}
	};

	static void show_weighted_image(const char *name, const WeightedImage &img);
}

using namespace multibandblender_impl;

class MultibandBlender::Impl {
	vector<SourceImage> m_input_image;

	shared_ptr<WeightedPixel> m_all_image_mem;
	//! memory pool for m_cur_level, m_next_level

	WeightedPixel *m_cur_level = nullptr, *m_next_level = nullptr;
	size_t m_level_size = 0;

	vector<int> m_target_stack, m_target_stack_idx;
	/*! 
	 * let a = m_target_stack_idx[i], b = m_target_stack_idx[i + 1],
	 *	then offset of source pixels on i'th pixel on target is:
	 *		m_target_stack[a], m_target_stack[a + 1], ...,
	 *		m_target_stack[b - 1]
	 */

	const MultibandBlender &m_blender;

	void alloc_pyramid_mem();
	void init_target_stack(int width, int height);
	void calc_next_level(int num);

	vector<WeightedImage> split_level_into_image(WeightedPixel *level);

	/*!
	 * \brief extend pixel values to area where weight = 0, to avoid dark edge
	 *		after gaussian blur
	 */
	void extend_edge(WeightedImage &img);

	void copysrc_and_recalc_weight(WeightedPixel *level);

	public:
		Impl(const MultibandBlender &blender):
			m_blender(blender)
		{}

		void add_image(const Point &upper_left,
				const Matrix<Vector2D> &orig_pos, const Image &img);

		void run(Image &target);
};

void MultibandBlender::Impl::run(Image &target) {
	alloc_pyramid_mem();
	init_target_stack(target.width(), target.height());
	copysrc_and_recalc_weight(m_cur_level);

	target.fill_memset(0);
	pixel_t *target_pxl = target.data();

	for (int lvl = 0; lvl <= m_blender.m_nr_band; lvl ++) {
		if (lvl == m_blender.m_nr_band)
			memset(m_next_level, 0, sizeof(WeightedPixel) * m_level_size);
		else
			calc_next_level(lvl);
		if (m_blender.m_show_band) {
			for (auto &b: split_level_into_image(m_cur_level))
				show_weighted_image("level", b);
		}
		for (int i = 0; i < target.area(); i ++) {
			real_t isum = 0, wsum = 0;
			int idx0 = m_target_stack_idx[i],
				idx1 = m_target_stack_idx[i + 1];
			if (idx0 < idx1) {
				for (int j = idx0; j < idx1; j ++) {
					int p = m_target_stack[j];
					auto &cur = m_cur_level[p], &next = m_next_level[p];
					real_t w = cur.weight;
					isum += w * (cur.intensity - next.intensity);
					wsum += w;
				}
				target_pxl[i] += isum / wsum;
			}
		}
		swap(m_cur_level, m_next_level);
	}
}

void MultibandBlender::Impl::calc_next_level(int num) {
	auto img = split_level_into_image(m_cur_level);
	FastGaussianFilter gaussian(sqrt(num * 2 + 1.0) * m_blender.m_sigma_step);
	size_t offset = 0;
	for (auto &i: img) {
		gaussian.run(i, m_next_level + offset);
		offset += i.area();
	}
	assert(offset == m_level_size);
}

void MultibandBlender::Impl::alloc_pyramid_mem() {
	m_level_size = 0;
	for (auto &i: m_input_image)
		m_level_size += i.img.area();
	m_all_image_mem = create_auto_buf<WeightedPixel>(m_level_size * 2);
	auto ptr = m_all_image_mem.get();
	m_cur_level = ptr;
	m_next_level = ptr + m_level_size;
}

vector<WeightedImage> MultibandBlender::Impl::split_level_into_image(
		WeightedPixel *level) {
	vector<WeightedImage> result;

	for (auto &i: m_input_image) {
		result.emplace_back(i.img.width(), i.img.height(),
				shared_ptr<WeightedPixel>(level, [](WeightedPixel *){}));
		level += i.img.area();
	}

	return result;
}

void MultibandBlender::Impl::init_target_stack(int width, int height) {
	m_target_stack.resize(m_level_size);
	m_target_stack_idx.resize(width * height + 1);

	int *idx_ptr = m_target_stack_idx.data();
	size_t mem_pos = 0;

	for (int i = 0; i < height; i ++)
		for (int j = 0; j < width; j ++) {
			*(idx_ptr ++) = mem_pos;
			size_t offset = 0;
			for (size_t img_idx = 0;
					img_idx < m_input_image.size(); img_idx ++) {
				auto &img = m_input_image[img_idx].img;
				auto &range = m_input_image[img_idx].range;
				int rs = i - range.xmin, cs = j - range.ymin;
				if (range.contain(i, j) && img.at(rs, cs).weight) {
					m_target_stack[mem_pos ++] =
						offset + rs * img.width() + cs;
				}
				offset += img.area();
			}
		}
	assert(mem_pos <= m_level_size);
	m_target_stack_idx.back() = mem_pos;
}

void MultibandBlender::Impl::add_image(const Point &upper_left,
		const Matrix<Vector2D> &orig_pos, const Image &img) {

	WeightedImage wimg;
	int width = orig_pos.width(), height = orig_pos.height();
	wimg.reset(width, height);
	for (int i = 0; i < height; i ++)
		for (int j = 0; j < width; j ++) {
			auto &p = orig_pos.at(i, j);
			auto &t = wimg.at(i, j);
			if (!p.is_nan()) {
				t.intensity = img.get_linear_interpolation_sc(p.x, p.y) + EPS;
				t.weight = (0.5 - fabs(p.x - 0.5)) * 
					(0.5 - fabs(p.y - 0.5)) + EPS;
				// +EPS to avoid == 0, used for ext
			}
		}
	extend_edge(wimg);
	m_input_image.emplace_back(wimg,
			Range2D(upper_left, width, height));
}

void MultibandBlender::Impl::extend_edge(WeightedImage &img) {
	std::queue<Point> queue;
	int width = img.width(), height = img.height();
	for (int i = 1; i + 1< height; i ++) {
		auto pxl = img.data(i, 0);
		for (int j = 1; j + 1 < width; j ++)
			if (pxl[j].intensity) {
				for (int d: {-width, -1, width, 1})
					if (!pxl[j + d].intensity) {
						queue.emplace(i, j);
						break;
					}
			}
	}
	while (!queue.empty()) {
		Point v0 = queue.front();
		auto intensity0 = img.at(v0.r, v0.c).intensity;
		queue.pop();
		for (int dr: {-1, 1})
			for (int dc: {-1, 1}) {
				auto pxl = img.at(v0.r + dr, v0.c + dc, nullptr);
				if (pxl && !pxl->intensity) {
					pxl->intensity = intensity0;
					queue.emplace(v0.r + dr, v0.c + dc);
				}
			}
	}
}

void MultibandBlender::Impl::copysrc_and_recalc_weight(WeightedPixel *level) {
	{
		size_t offset = 0;
		for (size_t i = 0; i < m_input_image.size(); i ++) {
			auto &src = m_input_image[i].img;
			memcpy(level + offset, src.data(),
					src.area() * sizeof(WeightedPixel));
			offset += src.area();
		}
	}
	for (size_t idxidx = 1; idxidx < m_target_stack_idx.size(); idxidx ++) {
		int idx0 = m_target_stack_idx[idxidx - 1],
			idx1 = m_target_stack_idx[idxidx];
		real_t maxw = numeric_limits<real_t>::lowest();
		for (int i = idx0; i < idx1; i ++)
			update_max(maxw, level[m_target_stack[i]].weight);
		maxw -= EPS;
		for (int i = idx0; i < idx1; i ++) {
			auto &t = level[m_target_stack[i]];
			t.weight = (t.weight >= maxw);
		}
	}
}

void multibandblender_impl::show_weighted_image(
		const char *name, const WeightedImage &img) {
	Image weight, intensity;
	weight.set_dimension(img.width(), img.height());
	intensity.set_dimension(img.width(), img.height());
	pixel_t *pw = weight.data(), *pi = intensity.data();
	for (auto &s: img) {
		*(pw ++) = s.weight;
		*(pi ++) = s.intensity;
	}

	static int num = 0;
	weight.save(DEBUG_DIR"%s-%.2d-weight.png", name, num);
	intensity.save(DEBUG_DIR"%s-%.2d-intensity.png", name, num);
	num ++;
}

MultibandBlender::MultibandBlender():
	m_impl(new Impl(*this)),
	m_nr_band(config.get_int("MULTIBAND.NR_BAND", 5)),
	m_sigma_step(config.get_double("MULTIBAND.SIGMA_STEP", 5))
{
}

MultibandBlender::~MultibandBlender() {
	delete m_impl;
}

void MultibandBlender::add_image(const Point &upper_left,
		const Matrix<Vector2D> &orig_pos, const Image &img) {
	m_impl->add_image(upper_left, orig_pos, img);
}

void MultibandBlender::run(Image &target) {
	m_impl->run(target);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

