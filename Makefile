# $File: Makefile
# $Date: Thu May 02 14:30:17 2013 +0800
# $Author: jiakai <jia.kai66@gmail.com>

BUILD_DIR = build
TARGET = istitcher


ARGS = data/out.png data/input*.png

CXX = g++ --std=c++11
IMGVIEW = gqview

SRC_EXT = cc
override CPPFLAGS += \
	-Isrc/include -Ilib $(shell Magick++-config --cppflags)
override OPTFLAG ?= -ggdb -Ofast

override CXXFLAGS += \
	-Wall -Wextra -Wnon-virtual-dtor -Wno-unused-parameter -Winvalid-pch \
	-Wno-unused-local-typedefs  -fopenmp -pthread \
	$(CPPFLAGS) $(OPTFLAG) \
	$(shell Magick++-config --cxxflags)
override LDFLAGS += -ggdb -fopenmp -pthread \
	$(shell Magick++-config --ldflags)

CXXSOURCES = $(shell find src -name "*.$(SRC_EXT)")
OBJS = $(addprefix $(BUILD_DIR)/,$(CXXSOURCES:.$(SRC_EXT)=.o))
DEPFILES = $(OBJS:.o=.d)


all: $(TARGET)

$(BUILD_DIR)/%.o: %.$(SRC_EXT)
	@echo "[cxx] $< ..."
	@$(CXX) -c $< -o $@ $(CXXFLAGS)

$(BUILD_DIR)/%.d: %.$(SRC_EXT)
	@mkdir -pv $(dir $@)
	@echo "[dep] $< ..."
	@$(CXX) $(CPPFLAGS) -MM -MT "$@ $(@:.d=.o)" "$<"  > "$@"

sinclude $(DEPFILES)

$(TARGET): $(OBJS)
	@echo "Linking ..."
	@$(CXX) $(OBJS) -o $@ $(LDFLAGS)

clean:
	rm -rf $(BUILD_DIR) $(TARGET) gmon.out

run: $(TARGET)
	rm -f data/debug/*
	./$(TARGET) $(ARGS)
	$(IMGVIEW) data/out.png

gdb:
	make debug
	gdb --args $(TARGET) $(ARGS)

debug:
	OPTFLAG='-O0 -ggdb' make -j4

gprof:
	OPTFLAG='-pg' LDFLAGS='-pg' make -j4

optimized:
	OPTFLAG='-Ofast' make -j4

.PHONY: all clean run gdb debug gprof optimized

# vim: ft=make

